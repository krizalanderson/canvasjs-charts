Export filter is either the FOCUS GROUP ID (focus_group.id or if by respondent respondent.id_focus_group) or the SITE ID (focus_group.id_site) = Whatever we chose for focus group or site

Heirarchy is Project > Site > Focus Group (select a project, select a site, select a focus group, then report at either the site or focus group level)

Sites have a "name" field to use as a label, projects have a "title" field but focus groups use a concatenation of "meeting_date", "community" and "venue" 

As appropriate point out that all monetary values have been converted to USD, all units of mass/weight have been converted to kg and all units of area have been converted to hectares

***

// Metadata for data set

    SQLQuery('SELECT '''+report_export_level+''' as report_level, "'''+report_export_label+'''" as report_title, '''+DateToStr(current_time)+''' as export_date, '''+TimeToStr(current_time)+''' as export_time FROM app_registration', Results);
    DatasetToXML(Results, 'Report\xml\report_metadata.xml');
    Results.Free;

// Focus Group Landholding Stats

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, landholding_category.description as farm_size, focus_group.threshold_small_farm_ha as small_threshold, focus_group.threshold_large_farm_ha as large_threshold FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN landholding_category ON respondent.id_landholding_category = landholding_category.id '+report_export_filter, Results);
    DatasetToXML(Results, 'Report\xml\report_landholding.xml');
    Results.Free;

// Livestock Holdings

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, landholding_category.description as farm_size, animal_species.description as species, animal_category.description as category, animal_type.description as type, TOTAL(livestock_holding.headcount) as head, TOTAL(livestock_holding.average_weight) as average_weight, TOTAL((livestock_holding.average_weight * livestock_holding.headcount)/250) as tlus, animal_type.lactating, animal_type.dairy FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN landholding_category ON respondent.id_landholding_category = landholding_category.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type LEFT OUTER JOIN animal_category ON animal_category.id = animal_type.id_animal_category LEFT OUTER JOIN animal_species ON animal_species.id = animal_category.id_animal_species WHERE livestock_holding.headcount IS NOT NULL '+report_export_filter_short+'GROUP BY animal_type.description', Results);
    DatasetToXML(Results, 'Report\xml\report_livestock_holdings.xml');
    Results.Free;

// Crop Cultivation in hectares and Residue Usage

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, crop_type.name as crop, TOTAL(crop_cultivation.cultivated_land * unit_area.conversion_ha) as hectares, TOTAL(crop_cultivation.annual_yield * unit_mass_weight.conversion_kg) as kilograms, TOTAL(percent_burned * 0.01) as residue_percent_burned, TOTAL(percent_fed * 0.01) as residue_percent_fed, TOTAL(percent_mulched * 0.01) as residue_percent_mulched, TOTAL(percent_sold * 0.01) as residue_percent_sold, TOTAL(percent_other * 0.01) as residue_percent_other FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type LEFT OUTER JOIN unit_area on crop_cultivation.id_unit_area = unit_area.id LEFT OUTER JOIN unit_mass_weight on crop_cultivation.id_unit_mass_weight = unit_mass_weight.id WHERE crop_cultivation.cultivated_land IS NOT NULL '+report_export_filter_short+' GROUP BY respondent.id, crop_type.name', Results);
    DatasetToXML(Results, 'Report\xml\report_crop_cultivation.xml');
    Results.Free;

// Fodder Crop Cultivation in hectares

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, fodder_crop_type.name as fodder_crop, TOTAL(fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha) as hectares FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type LEFT OUTER JOIN unit_area on fodder_crop_cultivation.id_unit_area = unit_area.id WHERE fodder_crop_cultivation.cultivated_land IS NOT NULL '+report_export_filter_short+'GROUP BY respondent.id, fodder_crop_type.name', Results);
    DatasetToXML(Results, 'Report\xml\report_fodder_cultivation.xml');
    Results.Free;

// Purchased Feed in kilograms

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, purchased_feed_type.name as feed, TOTAL(purchased_feed.quantity_purchased * purchased_feed.purchases_per_year * unit_mass_weight.conversion_kg) as kilograms, TOTAL(purchased_feed.unit_price * currency.current_usd_exchange_rate * purchased_feed.quantity_purchased * purchased_feed.purchases_per_year * unit_mass_weight.conversion_kg) AS expenditures FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id  LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN currency ON currency.id = site.id_currency LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type LEFT OUTER JOIN unit_mass_weight on purchased_feed.id_unit_mass_weight = unit_mass_weight.id WHERE purchased_feed.quantity_purchased IS NOT NULL '+report_export_filter_short+' GROUP BY respondent.id, purchased_feed_type.name', Results);
    DatasetToXML(Results, 'Report\xml\report_purchased_feed.xml');
    Results.Free;

// Income Sources

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, income_activity_category.description as category, income_activity_type.description as activity, COALESCE(TOTAL(income_activity.percent_of_hh_income * .01), 0) as percent FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id  LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN income_activity ON income_activity.id_respondent = respondent.id LEFT OUTER JOIN income_activity_type ON income_activity_type.id = income_activity.id_income_activity_type LEFT OUTER JOIN income_activity_category ON income_activity_category.id = income_activity_type.id_income_activity_category WHERE income_activity_type.description IS NOT NULL '+report_export_filter_short+'GROUP BY income_activity_category.description, income_activity_type.description', Results);
    DatasetToXML(Results, 'Report\xml\report_income_by_activity.xml');
    Results.Free;

// Monthly Rainfall

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, month.name as month_name, month.ordering, season.name as season_name, scale_zero_five.number AS rainfall FROM month LEFT OUTER JOIN focus_group_monthly_statistics ON focus_group_monthly_statistics.id_month = month.id LEFT OUTER JOIN season ON focus_group_monthly_statistics.id_season = season.id LEFT OUTER JOIN focus_group ON focus_group.id = focus_group_monthly_statistics.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN scale_zero_five ON focus_group_monthly_statistics.id_scale_zero_five = scale_zero_five.id WHERE scale_zero_five.number IS NOT NULL '+report_export_filter_short+'GROUP BY month.ordering', Results);
    DatasetToXML(Results, 'Report\xml\report_monthly_rainfall.xml');
    Results.Free;

// Monthly Feed Availablity

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, month.name as month_name, month.ordering, season.name as season_name, feed_source.description as source_of_feed, (IFNULL(feed_source_availability.percentage, 0) * scale_zero_ten.number * 0.01) as availability FROM month LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN scale_zero_ten ON respondent_monthly_statistics.id_scale_zero_ten = scale_zero_ten.id LEFT OUTER JOIN focus_group_monthly_statistics ON focus_group_monthly_statistics.id_month = month.id LEFT OUTER JOIN season ON focus_group_monthly_statistics.id_season = season.id LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN feed_source_availability ON feed_source_availability.id_month = month.id LEFT OUTER JOIN feed_source ON feed_source_availability.id_feed_source = feed_source.id '+report_export_filter+' GROUP BY respondent.id, month.ordering, feed_source.description', Results);
    DatasetToXML(Results, 'Report\xml\report_monthly_feed_availability.xml');
    Results.Free;

// Monthly Milk Statistics

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, month.name as month_name, month.ordering, month.days, AVG(respondent_monthly_statistics.milk_average_yield) as average_yield, AVG(respondent_monthly_statistics.milk_retained_for_household) as average_retained, AVG(respondent_monthly_statistics.milk_average_price_litre * currency.current_usd_exchange_rate) as price_litre_milk_usd, TOTAL(month.days * (respondent_monthly_statistics.milk_average_yield - respondent_monthly_statistics.milk_retained_for_household) * respondent_monthly_statistics.milk_average_price_litre * currency.current_usd_exchange_rate) as sales FROM month LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN currency ON currency.id = site.id_currency WHERE respondent_monthly_statistics.milk_average_yield IS NOT NULL '+report_export_filter_short+'GROUP BY focus_group.id, month.ordering', Results);
    DatasetToXML(Results, 'Report\xml\report_monthly_milk_stats.xml');
    Results.Free;

// Monthly Market Prices

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, month.name as month_name, (AVG(respondent_monthly_statistics.market_price_cattle) * currency.current_usd_exchange_rate) as price_head_cattle_usd, (AVG(respondent_monthly_statistics.market_price_sheep) * currency.current_usd_exchange_rate) as price_head_sheep_usd, (AVG(respondent_monthly_statistics.market_price_goat) * currency.current_usd_exchange_rate) as price_head_goat_usd FROM month LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN currency ON currency.id = site.id_currency WHERE (respondent_monthly_statistics.market_price_cattle IS NOT NULL OR respondent_monthly_statistics.market_price_goat IS NOT NULL OR respondent_monthly_statistics.market_price_sheep IS NOT NULL) '+report_export_filter_short+' GROUP BY focus_group.id, month.ordering', Results);
    DatasetToXML(Results, 'Report\xml\report_monthly_market_price.xml');
    Results.Free;

// Livestock Offtake

    SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, animal_species.description, gender.description, livestock_sale.number_sold, livestock_sale.approximate_weight FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_sale ON livestock_sale.id_respondent = respondent.id LEFT OUTER JOIN livestock_sale_category ON livestock_sale.id_livestock_sale_category = livestock_sale_category.id LEFT OUTER JOIN gender ON gender.id = livestock_sale_category.id_gender LEFT OUTER JOIN animal_species ON livestock_sale_category.id_animal_species = animal_species.id WHERE livestock_sale.number_sold IS NOT NULL '+report_export_filter_short+'GROUP BY animal_species.description, gender.description', Results);
    DatasetToXML(Results, 'Report\xml\report_livestock_offtake.xml');
    Results.Free;

 // Gender Pay Equality

     SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, AVG(labour_activity.daily_rate_female) as avg_pay_female, AVG(labour_activity.daily_rate_male) as avg_pay_male FROM focus_group LEFT OUTER JOIN labour_activity ON focus_group.id = labour_activity.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project WHERE labour_activity.daily_rate_female IS NOT NULL AND labour_activity.daily_rate_male IS NOT NULL '+report_export_filter_short+' GROUP BY focus_group.id', Results);
     DatasetToXML(Results, 'Report\xml\report_gender_pay_equality.xml');
     Results.Free;

 // Diet Composition

 SQLQuery('WITH crop_residue_stats AS (SELECT crop_cultivation.id as crop_id, (IFNULL((((crop_cultivation.annual_yield / crop_type.harvest_index) - crop_cultivation.annual_yield) * crop_cultivation.percent_fed * 0.01 * crop_type.content_percent_dry_matter * 0.01), 0)) as crop_residue_dm FROM crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = crop_cultivation.id_unit_area LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type),'+
             ' cultivated_fodder_stats AS (SELECT fodder_crop_cultivation.id as cultivated_fodder_id, (IFNULL((fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha * fodder_crop_type.annual_dry_matter_per_hectare), 0)) as cultivated_fodder_dm FROM fodder_crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = fodder_crop_cultivation.id_unit_area LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type),'+
             ' purchased_feed_stats AS (SELECT purchased_feed.id as purchased_feed_id,(IFNULL((purchased_feed.quantity_purchased * unit_mass_weight.conversion_kg * purchased_feed.purchases_per_year * purchased_feed_type.content_percent_dry_matter * 0.01), 0)) as purchased_feed_dm FROM purchased_feed LEFT OUTER JOIN unit_mass_weight ON unit_mass_weight.id = purchased_feed.id_unit_mass_weight LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type)'+
          'SELECT '+
             ' project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id,'+
             ' TOTAL(crop_residue_stats.crop_residue_dm) as dm_crop_residue, TOTAL(crop_residue_stats.crop_residue_dm * crop_type.content_metabolisable_energy) as me_crop_residue, TOTAL(crop_residue_stats.crop_residue_dm * crop_type.content_crude_protein * 0.01) as cp_crop_residue,'+
             ' TOTAL(cultivated_fodder_stats.cultivated_fodder_dm) as dm_cultivated_fodder, TOTAL(cultivated_fodder_stats.cultivated_fodder_dm * fodder_crop_type.content_metabolisable_energy) as me_cultivated_fodder, TOTAL(cultivated_fodder_stats.cultivated_fodder_dm * fodder_crop_type.content_crude_protein * 0.01) as cp_cultivated_fodder,'+
             ' TOTAL(purchased_feed_stats.purchased_feed_dm) as dm_purchased_feed, TOTAL(purchased_feed_stats.purchased_feed_dm * purchased_feed_type.content_metabolisable_energy) as me_purchased_feed, TOTAL(purchased_feed_stats.purchased_feed_dm * purchased_feed_type.content_crude_protein * 0.01) as cp_purchased_feed,'+
             ' TOTAL((IFNULL(respondent.diet_percent_grazing, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) as dm_grazing,'+
             ' TOTAL(((IFNULL(respondent.diet_percent_grazing, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.grazing_crude_protein_percentage * 0.01) as cp_grazing,'+
             ' TOTAL(((IFNULL(respondent.diet_percent_grazing, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.grazing_metabolisable_energy) as me_grazing,'+
             ' TOTAL((IFNULL(respondent.diet_percent_collected_fodder, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) as dm_collected_fodder,'+
             ' TOTAL(((IFNULL(respondent.diet_percent_collected_fodder, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.collected_fodder_crude_protein_percentage * 0.01) as cp_collected_fodder,'+
             ' TOTAL(((IFNULL(respondent.diet_percent_collected_fodder, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.collected_fodder_metabolisable_energy) as me_collected_fodder'+
     ' FROM respondent'+
             ' LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group'+
             ' LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_residue_stats ON crop_cultivation.id = crop_residue_stats.crop_id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type'+
             ' LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN cultivated_fodder_stats ON fodder_crop_cultivation.id = cultivated_fodder_stats.cultivated_fodder_id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type'+
             ' LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_stats ON purchased_feed.id = purchased_feed_stats.purchased_feed_id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type'+
             ' LEFT OUTER JOIN site ON focus_group.id_site = site.id LEFT OUTER JOIN project ON project.id = site.id_project '+report_export_filter+
             ' GROUP BY respondent.id', Results);

