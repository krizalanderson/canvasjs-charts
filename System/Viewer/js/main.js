window.onload = function() {

	var gui = require('nw.gui');

	var execArgs = gui.App.argv;

	var fs = require('fs');
	var sqlite3 = require('sqlite3');
	var path = require('path');
	var chart;
	var appRoot = path.dirname(process.execPath);

	var nwDir = path.normalize(appRoot + '/../../sqlite.db');

	var db = new sqlite3.Database(nwDir);

	db.serialize(function() {
		db.all('SELECT  fg.id AS focus_group_id, fg.venue_name AS focus_group_name, site.id AS site_id, site.name AS site_name, p.id AS project_id, p.title AS project_name FROM project p LEFT OUTER JOIN site ON site.id_project = p.id LEFT OUTER JOIN focus_group fg ON site.id = fg.id_site',
			function(err, rows) {
				var ddData = {
					projects: []
				};
				rows.sort(function(a, b) {
					var retVal = (a.project_name < b.project_name) ? -1 : 1;
					return retVal;
				});
				var pidIndex = [];
				var sidIndex = [];
				var fidIndex = [];
				rows.forEach(function(el) {
					console.log(el);
					var pid = el.project_id;
					var sid = el.site_id;
					var fid = el.focus_group_id;
					ddData[pid] = ddData[pid] || {
						project_name: el.project_name,
						sites: []
					};
					ddData[pid][sid] = ddData[pid][sid] || {
						site_name: el.site_name,
						focusGroups: []
					};
					if (pidIndex.indexOf(el.project_id) === -1) {
						pidIndex.push(el.project_id);
						ddData.projects.push({
							project_id: pid,
							project_name: el.project_name
						});
					}

					if (sidIndex.indexOf(el.site_id) === -1) {
						sidIndex.push(el.site_id);
						ddData[pid].sites.push({
							site_id: sid,
							site_name: el.site_name
						});
					}

					if (fidIndex.indexOf(el.focus_group_id) === -1) {
						fidIndex.push(el.focus_group_id);
						ddData[pid][sid].focusGroups.push({
							focus_group_id: fid,
							focus_group_name: el.focus_group_name
						});
					}
				});
				console.log(ddData);
				chartUi(ddData);
			}

		);
	});

	/*db.on('profile', function(statement){
	console.log('CHANGE EVENT');// - STATEMENT FOLLOWS:');
	//console.log(statement);
	} );*/

	var queries = {

		// Households by Landholding Category [COLUMN CHART]
		'households_by_landholding': {
			displayName: 'Households by Landholding Category',

			query: function(param, groupParam, callback) {
				var statement = "SELECT landholding_category.description AS farm_size, COUNT(*) as number_of_households FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN landholding_category ON respondent.id_landholding_category = landholding_category.id WHERE " + param + " GROUP BY landholding_category.description ORDER BY landholding_category.description DESC";
				fireStatement(statement, callback);
			}
		},

		// Livestock Holdings - Average Headcount per Household by Animal Type and Farm Size [COLUMN CHART]
		'livestock_holdings_1': {
			displayName: 'Livestock Holdings 1 - Average Headcount per Household by Animal Type and Farm Size',

			query: function(param, groupParam, callback) {
				var statement = "WITH resp_by_category AS (SELECT respondent.id_landholding_category AS cat_landholding, COUNT(*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " GROUP BY respondent.id_landholding_category) SELECT landholding_category.description AS farm_size, animal_type.description AS type_of_animal, (TOTAL(livestock_holding.headcount)/resp_by_category.number_of) AS average_headcount FROM respondent LEFT OUTER JOIN resp_by_category ON resp_by_category.cat_landholding = respondent.id_landholding_category LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN landholding_category ON respondent.id_landholding_category = landholding_category.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type WHERE livestock_holding.headcount IS NOT NULL AND " + param + " GROUP BY animal_type.description, landholding_category.description  ORDER BY average_headcount DESC";

				fireStatement(statement, callback);
			}
		},

		// Livestock Holdings 2 - Average TLUs per Household by Animal Type [COLUMN CHART]
		'livestock_holdings_2': {
			displayName: 'Livestock Holdings 2 - Average TLUs per Household by Animal Type',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT animal_type.description as type_of_animal, (TOTAL(livestock_holding.average_weight * livestock_holding.headcount)/(250*interview_respondents.number_of)) AS average_tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type LEFT OUTER JOIN animal_category ON animal_category.id = animal_type.id_animal_category LEFT OUTER JOIN animal_species ON animal_species.id = animal_category.id_animal_species LEFT OUTER JOIN interview_respondents WHERE livestock_holding.headcount IS NOT NULL AND " + param + " GROUP BY animal_type.description  ORDER BY average_tlus DESC";

				fireStatement(statement, callback);
			}
		},

		// Livestock Holdings 3 - Average TLUs per Household by Animal Category [COLUMN CHART]
		'livestock_holdings_3': {
			displayName: 'Livestock Holdings 3 - Average TLUs per Household by Animal Category',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT animal_category.description as category_of_animal, (TOTAL(livestock_holding.average_weight * livestock_holding.headcount)/(250*interview_respondents.number_of)) AS average_tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type LEFT OUTER JOIN animal_category ON animal_category.id = animal_type.id_animal_category LEFT OUTER JOIN animal_species ON animal_species.id = animal_category.id_animal_species LEFT OUTER JOIN interview_respondents WHERE livestock_holding.headcount IS NOT NULL AND " + param + " GROUP BY animal_category.description ORDER BY average_tlus DESC";

				fireStatement(statement, callback);
			}
		},

		// Livestock Holdings 4 - Dominant Animal Categories by Average TLUs per Household [COLUMN CHART]
		'livestock_holdings_4': {
			displayName: 'Livestock Holdings 4 - Dominant Animal Categories by Average TLUs per Household',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT animal_category.description as category_of_animal, (TOTAL(livestock_holding.average_weight * livestock_holding.headcount)/(250*interview_respondents.number_of)) AS average_tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type LEFT OUTER JOIN animal_category ON animal_category.id = animal_type.id_animal_category LEFT OUTER JOIN animal_species ON animal_species.id = animal_category.id_animal_species LEFT OUTER JOIN interview_respondents WHERE livestock_holding.headcount IS NOT NULL AND " + param + " GROUP BY animal_category.description ORDER BY average_tlus DESC LIMIT 5";

				fireStatement(statement, callback);
			}
		},

		// Crop Cultivation 1- Average Hectares Cultivated per Household by Crop Type [COLUMN CHART]
		'crop_cultivation_1': {
			displayName: 'Crop Cultivation 1- Average Hectares Cultivated per Household by Crop Type',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT crop_type.name as type_of_crop, (TOTAL(crop_cultivation.cultivated_land * unit_area.conversion_ha)/interview_respondents.number_of) as average_ha_per_hh FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN interview_respondents LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type LEFT OUTER JOIN unit_area on crop_cultivation.id_unit_area = unit_area.id WHERE crop_cultivation.cultivated_land IS NOT NULL AND " + param + " GROUP BY crop_type.name ORDER BY average_ha_per_hh DESC";

				fireStatement(statement, callback);
			}
		},

		// Crop Cultivation 2 - Dominant Crop Types by Average Hectares Cultivated per Household [COLUMN CHART]
		'crop_cultivation_2': {
			displayName: 'Crop Cultivation 2 - Dominant Crop Types by Average Hectares Cultivated per Household',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT crop_type.name as type_of_crop, (TOTAL(crop_cultivation.cultivated_land * unit_area.conversion_ha)/interview_respondents.number_of) as average_ha_per_hh FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN interview_respondents LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type LEFT OUTER JOIN unit_area on crop_cultivation.id_unit_area = unit_area.id WHERE crop_cultivation.cultivated_land IS NOT NULL AND " + param + " GROUP BY crop_type.name ORDER BY average_ha_per_hh DESC LIMIT 5";

				fireStatement(statement, callback);
			}
		},

		// Fodder Crop Cultivation 1 - Average Hectares Cultivated per Household by Fodder Crop Type [COLUMN CHART]
		'fodder_cultivation_1': {
			displayName: 'Fodder Crop Cultivation 1 - Average Hectares Cultivated per Household by Fodder Crop Type',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT fodder_crop_type.name as type_of_fodder_crop, (TOTAL(fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha)/interview_respondents.number_of) as average_ha_per_hh FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN interview_respondents LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type LEFT OUTER JOIN unit_area on fodder_crop_cultivation.id_unit_area = unit_area.id WHERE fodder_crop_cultivation.cultivated_land IS NOT NULL AND " + param + " GROUP BY fodder_crop_type.name ORDER BY average_ha_per_hh DESC";

				fireStatement(statement, callback);
			}
		},

		// Fodder Crop Cultivation 2 - Dominant Fodder Crop Types by Average Hectares Cultivated per Household [COLUMN CHART]
		'fodder_crop_2': {
			displayName: 'Fodder Crop Cultivation 2 - Dominant Fodder Crop Types by Average Hectares Cultivated per Household',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT fodder_crop_type.name as type_of_fodder_crop, (TOTAL(fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha)/interview_respondents.number_of) as average_ha_per_hh FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN interview_respondents LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type LEFT OUTER JOIN unit_area on fodder_crop_cultivation.id_unit_area = unit_area.id WHERE fodder_crop_cultivation.cultivated_land IS NOT NULL AND " + param + " GROUP BY fodder_crop_type.name ORDER BY average_ha_per_hh DESC LIMIT 5";

				fireStatement(statement, callback);
			}
		},

		// Purchased Feed 1 - Average Kilograms Purchased per Household by Feed Type [COLUMN CHART]
		'purchased_feed_1': {
			displayName: 'Purchased Feed 1 - Average Kilograms Purchased per Household by Feed Type',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT purchased_feed_type.name as type_of_feed, (TOTAL(purchased_feed.quantity_purchased * purchased_feed.purchases_per_year * unit_mass_weight.conversion_kg)/interview_respondents.number_of) as average_kg_per_hh FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id  LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type LEFT OUTER JOIN unit_mass_weight on purchased_feed.id_unit_mass_weight = unit_mass_weight.id LEFT OUTER JOIN interview_respondents WHERE purchased_feed.quantity_purchased IS NOT NULL AND " + param + " GROUP BY purchased_feed_type.name ORDER BY average_kg_per_hh DESC";

				fireStatement(statement, callback);
			}
		},

		// Purchased Feed 2 - Dominant Purchased Feed Types by Average Kilograms Purchased per Household [COLUMN CHART]
		'purchased_feed_2': {
			displayName: 'Purchased Feed 2 - Dominant Purchased Feed Types by Average Kilograms Purchased per Household',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT purchased_feed_type.name as type_of_feed, (TOTAL(purchased_feed.quantity_purchased * purchased_feed.purchases_per_year * unit_mass_weight.conversion_kg)/interview_respondents.number_of) as average_kg_per_hh FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id  LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type LEFT OUTER JOIN unit_mass_weight on purchased_feed.id_unit_mass_weight = unit_mass_weight.id LEFT OUTER JOIN interview_respondents WHERE purchased_feed.quantity_purchased IS NOT NULL AND " + param + " GROUP BY purchased_feed_type.name ORDER BY average_kg_per_hh DESC LIMIT 5";

				fireStatement(statement, callback);
			}
		},

		// Animal Diet and Nutrition 1 - Dry Matter Intake by Source [PIE CHART]
		'animal_diet_1': {
			displayName: 'Animal Diet and Nutrition 1 - Dry Matter Intake by Source',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id), crop_residue_stats AS (SELECT crop_cultivation.id as crop_id, (IFNULL((((crop_cultivation.annual_yield / crop_type.harvest_index) - crop_cultivation.annual_yield) * crop_cultivation.percent_fed * 0.01 * crop_type.content_percent_dry_matter * 0.01), 0)) as crop_residue_dm FROM crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = crop_cultivation.id_unit_area LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type), cultivated_fodder_stats AS (SELECT fodder_crop_cultivation.id as cultivated_fodder_id, (IFNULL((fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha * fodder_crop_type.annual_dry_matter_per_hectare), 0)) as cultivated_fodder_dm FROM fodder_crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = fodder_crop_cultivation.id_unit_area LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type), purchased_feed_stats AS (SELECT purchased_feed.id as purchased_feed_id,(IFNULL((purchased_feed.quantity_purchased * unit_mass_weight.conversion_kg * purchased_feed.purchases_per_year * purchased_feed_type.content_percent_dry_matter * 0.01), 0)) as purchased_feed_dm FROM purchased_feed LEFT OUTER JOIN unit_mass_weight ON unit_mass_weight.id = purchased_feed.id_unit_mass_weight LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type) SELECT TOTAL((crop_residue_stats.crop_residue_dm)/(interview_respondents.number_of)) as dm_crop_residue, TOTAL((cultivated_fodder_stats.cultivated_fodder_dm)/(interview_respondents.number_of)) as dm_cultivated_fodder, TOTAL((purchased_feed_stats.purchased_feed_dm)/(interview_respondents.number_of)) as dm_purchased_feed, TOTAL(((IFNULL(respondent.diet_percent_grazing, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)))/(interview_respondents.number_of)) as dm_grazing, TOTAL(((IFNULL(respondent.diet_percent_collected_fodder, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)))/(interview_respondents.number_of)) as dm_collected_fodder FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_residue_stats ON crop_cultivation.id = crop_residue_stats.crop_id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN cultivated_fodder_stats ON fodder_crop_cultivation.id = cultivated_fodder_stats.cultivated_fodder_id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_stats ON purchased_feed.id = purchased_feed_stats.purchased_feed_id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type LEFT OUTER JOIN site ON focus_group.id_site = site.id LEFT OUTER JOIN interview_respondents WHERE " + param + " GROUP BY " + groupParam + "";

				fireStatement(statement, callback);
			}
		},

		// Animal Diet and Nutrition 2 - Metabolisable Energy Intake by Source [PIE CHART]
		'animal_diet_2': {
			displayName: 'Animal Diet and Nutrition 2 - Metabolisable Energy Intake by Source',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id), crop_residue_stats AS (SELECT crop_cultivation.id as crop_id, (IFNULL((((crop_cultivation.annual_yield / crop_type.harvest_index) - crop_cultivation.annual_yield) * crop_cultivation.percent_fed * 0.01 * crop_type.content_percent_dry_matter * 0.01), 0)) as crop_residue_dm FROM crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = crop_cultivation.id_unit_area LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type), cultivated_fodder_stats AS (SELECT fodder_crop_cultivation.id as cultivated_fodder_id, (IFNULL((fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha * fodder_crop_type.annual_dry_matter_per_hectare), 0)) as cultivated_fodder_dm FROM fodder_crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = fodder_crop_cultivation.id_unit_area LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type), purchased_feed_stats AS (SELECT purchased_feed.id as purchased_feed_id,(IFNULL((purchased_feed.quantity_purchased * unit_mass_weight.conversion_kg * purchased_feed.purchases_per_year * purchased_feed_type.content_percent_dry_matter * 0.01), 0)) as purchased_feed_dm FROM purchased_feed LEFT OUTER JOIN unit_mass_weight ON unit_mass_weight.id = purchased_feed.id_unit_mass_weight LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type) SELECT TOTAL((crop_residue_stats.crop_residue_dm * crop_type.content_metabolisable_energy)/(interview_respondents.number_of)) as me_crop_residue, TOTAL((cultivated_fodder_stats.cultivated_fodder_dm * fodder_crop_type.content_metabolisable_energy)/(interview_respondents.number_of)) as me_cultivated_fodder, TOTAL((purchased_feed_stats.purchased_feed_dm * purchased_feed_type.content_metabolisable_energy)/(interview_respondents.number_of)) as me_purchased_feed, TOTAL((((IFNULL(respondent.diet_percent_grazing, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.grazing_metabolisable_energy)/(interview_respondents.number_of)) as me_grazing, TOTAL((((IFNULL(respondent.diet_percent_collected_fodder, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.collected_fodder_metabolisable_energy)/(interview_respondents.number_of)) as me_collected_fodder FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_residue_stats ON crop_cultivation.id = crop_residue_stats.crop_id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN cultivated_fodder_stats ON fodder_crop_cultivation.id = cultivated_fodder_stats.cultivated_fodder_id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_stats ON purchased_feed.id = purchased_feed_stats.purchased_feed_id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type LEFT OUTER JOIN site ON focus_group.id_site = site.id LEFT OUTER JOIN interview_respondents WHERE " + param + " GROUP BY " + groupParam + "";

				fireStatement(statement, callback);
			}
		},

		// Animal Diet and Nutrition 3 - Crude Protein Intake by Source [PIE CHART]
		'animal_diet_3': {
			displayName: 'Animal Diet and Nutrition 3 - Crude Protein Intake by Source',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id), crop_residue_stats AS (SELECT crop_cultivation.id as crop_id, (IFNULL((((crop_cultivation.annual_yield / crop_type.harvest_index) - crop_cultivation.annual_yield) * crop_cultivation.percent_fed * 0.01 * crop_type.content_percent_dry_matter * 0.01), 0)) as crop_residue_dm FROM crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = crop_cultivation.id_unit_area LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type), cultivated_fodder_stats AS (SELECT fodder_crop_cultivation.id as cultivated_fodder_id, (IFNULL((fodder_crop_cultivation.cultivated_land * unit_area.conversion_ha * fodder_crop_type.annual_dry_matter_per_hectare), 0)) as cultivated_fodder_dm FROM fodder_crop_cultivation LEFT OUTER JOIN unit_area ON unit_area.id = fodder_crop_cultivation.id_unit_area LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type), purchased_feed_stats AS (SELECT purchased_feed.id as purchased_feed_id,(IFNULL((purchased_feed.quantity_purchased * unit_mass_weight.conversion_kg * purchased_feed.purchases_per_year * purchased_feed_type.content_percent_dry_matter * 0.01), 0)) as purchased_feed_dm FROM purchased_feed LEFT OUTER JOIN unit_mass_weight ON unit_mass_weight.id = purchased_feed.id_unit_mass_weight LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type) SELECT TOTAL((crop_residue_stats.crop_residue_dm * crop_type.content_crude_protein * 0.01)/(interview_respondents.number_of)) as cp_crop_residue, TOTAL((cultivated_fodder_stats.cultivated_fodder_dm * fodder_crop_type.content_crude_protein * 0.01)/(interview_respondents.number_of)) as cp_cultivated_fodder, TOTAL((purchased_feed_stats.purchased_feed_dm * purchased_feed_type.content_crude_protein * 0.01)/(interview_respondents.number_of)) as cp_purchased_feed, TOTAL((((IFNULL(respondent.diet_percent_grazing, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.grazing_crude_protein_percentage * 0.01)/(interview_respondents.number_of)) as cp_grazing, TOTAL((((IFNULL(respondent.diet_percent_collected_fodder, 0) * 0.01) * ((IFNULL(purchased_feed_stats.purchased_feed_dm, 0)) + (IFNULL(crop_residue_stats.crop_residue_dm, 0)) + (IFNULL(cultivated_fodder_stats.cultivated_fodder_dm, 0))) /(1-((IFNULL(respondent.diet_percent_grazing, 0) + IFNULL(respondent.diet_percent_collected_fodder, 0))*0.01)) ) * site.collected_fodder_crude_protein_percentage * 0.01)/(interview_respondents.number_of)) as cp_collected_fodder FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN crop_cultivation ON crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN crop_residue_stats ON crop_cultivation.id = crop_residue_stats.crop_id LEFT OUTER JOIN crop_type ON crop_type.id = crop_cultivation.id_crop_type LEFT OUTER JOIN fodder_crop_cultivation ON fodder_crop_cultivation.id_respondent = respondent.id LEFT OUTER JOIN cultivated_fodder_stats ON fodder_crop_cultivation.id = cultivated_fodder_stats.cultivated_fodder_id LEFT OUTER JOIN fodder_crop_type ON fodder_crop_type.id = fodder_crop_cultivation.id_fodder_crop_type LEFT OUTER JOIN purchased_feed ON purchased_feed.id_respondent = respondent.id LEFT OUTER JOIN purchased_feed_stats ON purchased_feed.id = purchased_feed_stats.purchased_feed_id LEFT OUTER JOIN purchased_feed_type ON purchased_feed_type.id = purchased_feed.id_purchased_feed_type LEFT OUTER JOIN site ON focus_group.id_site = site.id LEFT OUTER JOIN interview_respondents WHERE " + param + " GROUP BY " + groupParam + "";

				fireStatement(statement, callback);
			}
		},

		// Income Sources [PIE CHART]
		'income_sources': {
			displayName: 'Income Sources',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + " ORDER BY respondent.id) SELECT income_activity_category.description as income_category, TOTAL(COALESCE((income_activity.percent_of_hh_income * .01), 0)/interview_respondents.number_of) as average_percent FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id  LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN income_activity ON income_activity.id_respondent = respondent.id LEFT OUTER JOIN income_activity_type ON income_activity_type.id = income_activity.id_income_activity_type LEFT OUTER JOIN income_activity_category ON income_activity_category.id = income_activity_type.id_income_activity_category LEFT OUTER JOIN interview_respondents WHERE income_activity_type.description IS NOT NULL AND " + param + " GROUP BY income_activity_category.description";

				fireStatement(statement, callback);
			}
		},

		// Monthly Rainfall and Feed Availability [STACKED COLUMN CHART WITH RAINFALL SERIES REPRESENTED AS A LINE - RAIN ON AN AXIS FROM 0 TO 5, OTHER SERIES ON AN AXIS FROM 0 TO 10]
		'monthly_rainfall': {
			displayName: 'Monthly Rainfall and Feed Availability',

			query: function(param, groupParam, callback) {
				var statement = "WITH interview_respondents AS (SELECT COUNT (*) AS number_of FROM respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE " + param + "), " +
					"feed_source_values AS (SELECT month.ordering as month_order, month.name as name_of_month, month.ordering as order_of_month, feed_source.description as resource_type, ((IFNULL(feed_source_availability.percentage, 0) * scale_zero_ten.number * 0.01)/interview_respondents.number_of) as numerical_value FROM respondent INNER JOIN feed_source_availability ON respondent.id = feed_source_availability.id_respondent INNER JOIN month ON month.id = feed_source_availability.id_month INNER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_respondent = respondent.id INNER JOIN scale_zero_ten ON respondent_monthly_statistics.id_scale_zero_ten = scale_zero_ten.id LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN focus_group_monthly_statistics ON focus_group_monthly_statistics.id_month = month.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN feed_source ON feed_source_availability.id_feed_source = feed_source.id LEFT OUTER JOIN interview_respondents WHERE respondent_monthly_statistics.id_month = feed_source_availability.id_month AND focus_group.id = 3 GROUP BY respondent.id, month.id, feed_source.description ORDER BY month.ordering, feed_source.description) " +
					"SELECT month.name AS name_of_month, month.ordering AS order_of_month, 'Rainfall' as resource_type, AVG(scale_zero_five.number) AS numerical_value FROM month LEFT OUTER JOIN focus_group_monthly_statistics ON focus_group_monthly_statistics.id_month = month.id LEFT OUTER JOIN focus_group ON focus_group.id = focus_group_monthly_statistics.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN scale_zero_five ON focus_group_monthly_statistics.id_scale_zero_five = scale_zero_five.id WHERE scale_zero_five.number IS NOT NULL AND " + param + " GROUP BY month.name " +
					"UNION ALL " +
					"SELECT feed_source_values.name_of_month AS name_of_month, feed_source_values.month_order AS order_of_month, feed_source_values.resource_type AS resource_type, TOTAL(feed_source_values.numerical_value) " +
					"FROM feed_source_values GROUP BY feed_source_values.name_of_month, feed_source_values.resource_type " +
					"ORDER BY month.ordering"

				fireStatement(statement, callback);
			}
		},

		// Monthly Milk Yield versus Average Price Received  [LINE GRAPH - TWO DIFFERENT AXES FOR YIELD AND PRICE]
		'monthly_milk_yield': {
			displayName: 'Monthly Milk Yield versus Average Price Received',

			query: function(param, groupParam, callback) {
				var statement = "SELECT month.name as month_name, month.ordering, AVG(respondent_monthly_statistics.milk_average_yield * month.days) as average_yield, AVG(respondent_monthly_statistics.milk_average_price_litre * currency.current_usd_exchange_rate) as average_price_litre_usd FROM month LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN currency ON currency.id = site.id_currency WHERE respondent_monthly_statistics.milk_average_yield IS NOT NULL AND " + param + " GROUP BY focus_group.id, month.ordering ORDER BY month.ordering";

				fireStatement(statement, callback);
			}
		},

		// Monthly Market Prices for Livestock [LINE GRAPH - TWO DIFFERENT AXES FOR HEADCOUNT AND PRICE]
		'monthly_market_prices': {
			displayName: 'Monthly Market Prices',

			query: function(param, groupParam, callback) {
				var statement = "SELECT month.name as month_name, month.ordering, (AVG(respondent_monthly_statistics.market_price_cattle) * currency.current_usd_exchange_rate) as price_head_cattle_usd, (AVG(respondent_monthly_statistics.market_price_sheep) * currency.current_usd_exchange_rate) as price_head_sheep_usd, (AVG(respondent_monthly_statistics.market_price_goat) * currency.current_usd_exchange_rate) as price_head_goat_usd FROM month LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN currency ON currency.id = site.id_currency WHERE (respondent_monthly_statistics.market_price_cattle IS NOT NULL OR respondent_monthly_statistics.market_price_goat IS NOT NULL OR respondent_monthly_statistics.market_price_sheep IS NOT NULL) AND " + param + " GROUP BY focus_group.id, month.ordering ORDER BY month.ordering";

				fireStatement(statement, callback);
			}
		},

		// Gender Pay Equality [COLUMN CHART]
		'gender_pay_equality': {
			displayName: 'Gender Pay Equality',

			query: function(param, groupParam, callback) {
				var statement = "SELECT AVG(labour_activity.daily_rate_female) as avg_pay_female, AVG(labour_activity.daily_rate_male) as avg_pay_male FROM focus_group LEFT OUTER JOIN labour_activity ON focus_group.id = labour_activity.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site WHERE labour_activity.daily_rate_female IS NOT NULL AND labour_activity.daily_rate_male IS NOT NULL AND " + param + " GROUP BY focus_group.id";

				fireStatement(statement, callback);
			}
		}

		// Key Statistics [NOT FINISHED - WILL SEND LATER]
		/*   'key_statistics':{
		displayName:'Key Statistics',

		query:function(param, groupParam, callback){
		var statement = "WITH offtake_cattle AS (SELECT ((livestock_sale.number_sold * 1.0 * livestock_sale.approximate_weight)/250)) AS tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN livestock_sale ON livestock_sale.id_respondent = respondent.id LEFT OUTER JOIN livestock_sale_category ON livestock_sale.id_livestock_sale_category = livestock_sale_category.id LEFT OUTER JOIN animal_species ON livestock_sale_category.id_animal_species = animal_species.id WHERE livestock_sale.number_sold IS NOT NULL AND animal_species.description = 'Cattle' AND [[[ATTRIBUTES]]]) GROUP BY animal_species.description, gender.description), " +

		"offtake_shoat AS (SELECT ((livestock_sale.number_sold * livestock_sale.approximate_weight)/250)) AS tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN livestock_sale ON livestock_sale.id_respondent = respondent.id LEFT OUTER JOIN livestock_sale_category ON livestock_sale.id_livestock_sale_category = livestock_sale_category.id LEFT OUTER JOIN animal_species ON livestock_sale_category.id_animal_species = animal_species.id WHERE livestock_sale.number_sold IS NOT NULL AND animal_species.description = 'Sheep' OR 'Goat' AND [[[ATTRIBUTES]]]) GROUP BY animal_species.description, gender.description), " +
		"income_milk AS (SELECT TOTAL(month.days * (respondent_monthly_statistics.milk_average_yield - respondent_monthly_statistics.milk_retained_for_household) * respondent_monthly_statistics.milk_average_price_litre) as sales_local_currency LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN currency ON currency.id = site.id_currency WHERE respondent_monthly_statistics.milk_average_yield IS NOT NULL AND " + param + " GROUP BY " + groupParam + ")" +
		"cattle_data AS (SELECT (TOTAL(livestock_holding.average_weight * livestock_holding.headcount)/(250)) AS tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type LEFT OUTER JOIN animal_category ON animal_category.id = animal_type.id_animal_category LEFT OUTER JOIN animal_species ON animal_species.id = animal_category.id_animal_species WHERE livestock_holding.headcount IS NOT NULL AND animal_species.description = 'Cattle' AND " + param + " GROUP BY animal_category.description)" +
		"shoat_data AS (SELECT (TOTAL(livestock_holding.average_weight * livestock_holding.headcount)/(250)) AS tlus FROM respondent LEFT OUTER JOIN focus_group ON respondent.id_focus_group = focus_group.id LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN livestock_holding ON livestock_holding.id_respondent = respondent.id LEFT OUTER JOIN animal_type ON animal_type.id = livestock_holding.id_animal_type LEFT OUTER JOIN animal_category ON animal_category.id = animal_type.id_animal_category LEFT OUTER JOIN animal_species ON animal_species.id = animal_category.id_animal_species WHERE livestock_holding.headcount IS NOT NULL AND animal_species.description = ('Sheep' OR 'Goat') AND " + param + " GROUP BY animal_category.description)" +
		"SELECT ((offtake_cattle.tlus/(offtake_cattle.tlus + cattle_data.tlus))*100) as percent_cattle_offtake FROM cattle_data LEFT OUTER JOIN offtake_cattle" 

		SQLQuery('SELECT project.id as project_id, site.id as site_id, focus_group.id as focus_group_id, respondent.id as respondent_id, month.name as month_name, month.ordering, month.days, AVG(respondent_monthly_statistics.milk_average_yield) as average_yield, AVG(respondent_monthly_statistics.milk_retained_for_household) as average_retained, AVG(respondent_monthly_statistics.milk_average_price_litre * currency.current_usd_exchange_rate) as price_litre_milk_usd, TOTAL(month.days * (respondent_monthly_statistics.milk_average_yield - respondent_monthly_statistics.milk_retained_for_household) * respondent_monthly_statistics.milk_average_price_litre * currency.current_usd_exchange_rate) as sales FROM month LEFT OUTER JOIN respondent_monthly_statistics ON respondent_monthly_statistics.id_month = month.id LEFT OUTER JOIN respondent ON respondent.id = respondent_monthly_statistics.id_respondent LEFT OUTER JOIN focus_group ON focus_group.id = respondent.id_focus_group LEFT OUTER JOIN site ON site.id = focus_group.id_site LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN currency ON currency.id = site.id_currency WHERE respondent_monthly_statistics.milk_average_yield IS NOT NULL '+report_export_filter_short+'GROUP BY focus_group.id, month.ordering', Results);


		SELECT  as  FROM respondent  LEFT OUTER JOIN project ON project.id = site.id_project LEFT OUTER JOIN gender ON gender.id = livestock_sale_category.id_gender LEFT OUTER JOIN animal_species ON livestock_sale_category.id_animal_species = animal_species.id WHERE livestock_sale.number_sold IS NOT NULL '+report_export_filter_short+'GROUP BY animal_species.description, gender.description'

		/*query_name:{
		displayName:'Query',

		query:function(param, groupParam, callback){
		var statement = "' + buildFilter(param) +' <more sql here>';
		param = buildFilter(param);
		if(groupParam){ groupParam = buildGroupFilter(groupParam); }
		fireStatement(statement, callback);
		}
		} */

	};

	//console.log('exec args:'); console.log(execArgs);

	var argLen = execArgs.length; // so we can work backwards when executing from an external nw
	var query = execArgs[argLen - 3];
	var paramType = execArgs[argLen - 2];
	var paramVal = execArgs[argLen - 1];

	if (paramType === 'byFocusGroup') {
		$('#btn_by_group').addClass('active');
	} else {
		$('#btn_by_site').addClass('active');
	}

	function getMonth(year, month) {
				switch(month){
					case "January":
						return new Date(2015, 00);
						break;
					case "February":
						return new Date(2015, 01);
						break;
					case "March":
						return new Date(2015, 02);
						break;
					case "April":
						return new Date(2015, 03);
						break;
					case "May":
						return new Date(2015, 04);
						break;
					case "June":
						return new Date(2015, 05);
						break;
					case "July":
						return new Date(2015, 06);
						break;
					case "August":
						return new Date(2015, 07);
						break;
					case "September":
						return new Date(2015, 08);
						break;
					case "October":
						return new Date(2015, 09);
						break;
					case "November":
						return new Date(2015, 10);
						break;
					case "December":
						return new Date(2015, 11);
						break;
					default:
						return false
						break;
				}
			}

	var resultBindings = {
		households_by_landholding: function(err, rows) {
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].farm_size,
									y:rows[i].number_of_households
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				title: {
					text: "Count of Households by Landholding Category"
				},
				axisY: {
					title: "Number of Households"
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#99FF66",
					legendText: "Landholding Category",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);

		},

		// Livestock Holdings - Average Headcount per Household by Animal Type and Farm Size [COLUMN CHART]
		livestock_holdings_1: function(err, rows) {
			
			var holdingsData = [
				{
					type: "column",	
					name: "Large Farm",
					legendText: "Large Farm",
					showInLegend: true,
					color: "#CC3300",
					dataPoints: []
				},{
					type: "column",	
					name: "Medium Farm",
					legendText: "Medium Farm",
					showInLegend: true,
					color: "#FFCC00",
					dataPoints: []
				},{
					type: "column",	
					name: "Small Farm",
					legendText: "Small Farm",
					showInLegend: true,
					color: "#47B56C",
					dataPoints: []
				}
			];

			/** CREATE DATA ARRAY HERE */
			for (var i = 0; i < rows.length; i++) {
				switch(rows[i].farm_size){
					case "Large Farm":
						holdingsData[0].dataPoints.push({label: rows[i].type_of_animal, y: rows[i].average_headcount})
						break;
					case "Medium Farm":
						holdingsData[1].dataPoints.push({label: rows[i].type_of_animal, y: rows[i].average_headcount})
						break;
					case "Small Farm":
						holdingsData[2].dataPoints.push({label: rows[i].type_of_animal, y: rows[i].average_headcount})
						break;
					default:
						break;
				}
			};


			var dataObj = {
				theme: "theme2",
							animationEnabled: true,
				title:{
					text: "Average Livestock Holdings per Household by Farm Size (Headcount)",
					fontSize: 30
				},
				toolTip: {
					shared: true
				},
				axisY: {
					title: "Headcount"
				},
				axisX: {
					labelFontStyle: "italic",
					labelAutoFit: true,
					labelAngle: -45
				},
				legend:{
					verticalAlign: "top",
					horizontalAlign: "center"
				},
				data: holdingsData,
				legend: {
					cursor: "pointer",
					itemclick: function(e) {
						if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
							e.dataSeries.visible = false;
						} else {
							e.dataSeries.visible = true;
						}
						chart.render();
					}
				}
			}

			renderChart(dataObj);
		},

		// Livestock Holdings 2 - Average TLUs per Household by Animal Type [COLUMN CHART]
		livestock_holdings_2: function(err, rows) {
			//format rows into dataObj here

			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_animal,
									y:rows[i].average_tlus
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				title: {
					text: "Average Household Livestock Holdings by Type in Tropical Livestock Units (TLUs)"
				},
				axisY: {
					title: "TLUs"
				},
				axisX: {
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#3366CC",
					width: 50,
					legendText: "Landholding Category",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);

		},

		// Livestock Holdings 3 - Average TLUs per Household by Animal Category [COLUMN CHART]
		livestock_holdings_3: function(err, rows) {

			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].category_of_animal,
									y:rows[i].average_tlus
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				title: {
					text: "Average Household Livestock Holdings by Category in Tropical Livestock Units (TLUs)"
				},
				axisY: {
					title: "TLUs"
				},
				axisX: {
					labelWrap: true
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#78AFE6",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);

		},

		// Livestock Holdings 4 - Dominant Animal Categories by Average TLUs per Household [COLUMN CHART]
		livestock_holdings_4: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].category_of_animal,
									y:rows[i].average_tlus
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				title: {
					text: "Dominant Livestock Categories by TLUs (Up to 5)"
				},
				axisY: {
					title: "TLUs"
				},
				axisX: {
					labelWrap: true,
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#FF9900",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Crop Cultivation 1- Average Hectares Cultivated per Household by Crop Type [COLUMN CHART]
		crop_cultivation_1: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_crop,
									y:rows[i].average_ha_per_hh
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				height: 350,
				title: {
					text: "Average Hectares Cultivated per Household by Crop Type"
				},
				axisY: {
					title: "Hectares Under Cultivation",
					titleFontSize: 12
				},
				axisX: {
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#78AFE6",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Crop Cultivation 2 - Dominant Crop Types by Average Hectares Cultivated per Household [COLUMN CHART]
		crop_cultivation_2: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_crop,
									y:rows[i].average_ha_per_hh
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				height: 350,
				title: {
					text: "Dominant Crop Types by Average Hectares Cultivated (Up to 5)"
				},
				axisY: {
					title: "Hectares Under Cultivation",
					titleFontSize: 12
				},
				axisX: {
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#FF9900",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Fodder Crop Cultivation 1 - Average Hectares Cultivated per Household by Fodder Crop Type [COLUMN CHART]
		fodder_cultivation_1: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_fodder_crop,
									y:rows[i].average_ha_per_hh
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				height: 350,
				title: {
					text: "Average Hectares Cultivated per Household by Fodder Crop Type"
				},
				axisY: {
					title: "Hectares Under Cultivation",
					titleFontSize: 10
				},
				axisX: {
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#47B56C",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Fodder Crop Cultivation 2 - Dominant Fodder Crop Types by Average Hectares Cultivated per Household [COLUMN CHART]
		fodder_crop_2: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_fodder_crop,
									y:rows[i].average_ha_per_hh
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				height: 350,
				title: {
					text: "Dominant Fodder Crops by Average Hectares Cultivated (Up to 5)"
				},
				axisY: {
					title: "Hectares Under Cultivation",
					titleFontSize: 10
				},
				axisX: {
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#FF9900",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Purchased Feed 1 - Average Kilograms Purchased per Household by Feed Type [COLUMN CHART]
		purchased_feed_1: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_feed,
									y:rows[i].average_kg_per_hh
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				height: 400,
				title: {
					text: "Average kg of Feed Purchased per Household by Feed Type"
				},
				axisY: {
					title: "Kg Purchased Per Year",
					titleFontSize: 10
				},
				axisX: {
					labelFontStyle: "italic",
					labelFontSize: 12,
					labelMaxWidth: 150,  
					labelAutoFit: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#FF9900",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Purchased Feed 2 - Dominant Purchased Feed Types by Average Kilograms Purchased per Household [COLUMN CHART]
		purchased_feed_2: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);
			//format rows into dataObj here
			
			/** CREATE DATA POINTS */
			var dataPointsArray = [];

			for (var i = 0; i < rows.length; i++) {
				dataPointsArray.push({
									label: rows[i].type_of_feed,
									y:rows[i].average_kg_per_hh
							});
			};

			/** Create data object */
			var dataObj = {
				theme: "theme2", //theme1
				animationEnabled: true,
				height: 400,
				title: {
					text: "Dominant Purchased Feed Types by kg Purchased (Up to 5)"
				},
				axisY: {
					title: "Kg Purchased Per Year",
					titleFontSize: 10
				},
				axisX: {
					labelFontStyle: "italic",
					labelFontSize: 12,
					labelMaxWidth: 150,  
					labelAutoFit: true,
					labelAngle: -45
				},
				animationEnabled: true,
				data: [{
					// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
					type: "column",
					color: "#78AFE6",
					dataPoints: dataPointsArray
				}]
			}

			renderChart(dataObj);
		},

		// Animal Diet and Nutrition 1 - Dry Matter Intake by Source [PIE CHART]
		animal_diet_1: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);

			// Create data array like this
			var dataPointsArray = [
							{  y: rows[0].dm_collected_fodder, label: "Collected fodder", color:'#78AFE6'},
							{  y: rows[0].dm_cultivated_fodder, label: "Cultivated fodder", color:'#FF9900'},
							{  y: rows[0].dm_crop_residue, label: "Crop residue", color:'#47B56C'},
							{  y: rows[0].dm_purchased_feed, label: "Purchased feed", color:'#99FF66'},
							{  y: rows[0].dm_grazing, label: "Grazing", color:'#CC3300'}
						];

			var dataObj = {
				title:{
					text: "Dry Matter Intake by Source"
				},
				animationEnabled: true,
				legend:{
					verticalAlign: "center",
					horizontalAlign: "left",
					fontSize: 20,
					fontFamily: "Helvetica"        
				},
				theme: "theme2",
				data: [
					{        
						type: "pie",       
						indexLabelFontFamily: "verdana",       
						indexLabelFontSize: 12,
						indexLabel: "{label} {y}%",
						startAngle:-20,      
						toolTipContent:"{label} {y}%",
						dataPoints: dataPointsArray
					}
				]
			};

			renderChart(dataObj);
		},

		// Animal Diet and Nutrition 2 - Metabolisable Energy Intake by Source [PIE CHART]
		animal_diet_2: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);

			// Create data array like this
			var dataPointsArray = [
							{  y: 30, label: "Collected fodder", color:'#78AFE6'},
							{  y: 40, label: "Cultivated fodder", color:'#FF9900'},
							{  y: 20, label: "Crop residue", color:'#47B56C'},
							{  y: 4, label: "Purchased feed", color:'#99FF66'},
							{  y: 6, label: "Grazing", color:'#CC3300'}
						];

			var dataObj = {
				title:{
					text: "Metabolisable Energy Intake by Source"
				},
				animationEnabled: true,
				legend:{
					verticalAlign: "center",
					horizontalAlign: "left",
					fontSize: 20,
					fontFamily: "Helvetica"        
				},
				theme: "theme2",
				data: [
					{        
						type: "pie",       
						indexLabelFontFamily: "verdana",       
						indexLabelFontSize: 12,
						indexLabel: "{label} {y}%",
						startAngle:-20,      
						toolTipContent:"{label} {y}%",
						dataPoints: dataPointsArray
					}
				]
			};

			renderChart(dataObj);
		},

		// Animal Diet and Nutrition 3 - Crude Protein Intake by Source [PIE CHART]
		animal_diet_3: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);

			// Create data array like this
			var dataPointsArray = [
							{  y: 10, label: "Collected fodder", color:'#78AFE6'},
							{  y: 30, label: "Cultivated fodder", color:'#FF9900'},
							{  y: 10, label: "Crop residue", color:'#47B56C'},
							{  y: 21, label: "Purchased feed", color:'#99FF66'},
							{  y: 29, label: "Grazing", color:'#CC3300'}
						];

			var dataObj = {
				title:{
					text: "Crude Protein Intake by Source"
				},
				animationEnabled: true,
				legend:{
					verticalAlign: "center",
					horizontalAlign: "left",
					fontSize: 20,
					fontFamily: "Helvetica"        
				},
				theme: "theme2",
				data: [
					{        
						type: "pie",       
						indexLabelFontFamily: "verdana",       
						indexLabelFontSize: 12,
						indexLabel: "{label} {y}%",
						startAngle:-20,      
						toolTipContent:"{label} {y}%",
						dataPoints: dataPointsArray
					}
				]
			};

			renderChart(dataObj);
		},

		// Income Sources [PIE CHART]
		income_sources: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);

			// Create data array like this
			var dataPointsArray = [
							{  y: 44, label: "Live Stock", color:'#FFC000'},
							{  y: 3, label: "Remittance", color:'#4472C4'},
							{  y: 21, label: "Business", color:'#5B9BD5'},
							{  y: 29, label: "Cropping", color:'#ED7D31'},
							{  y: 4, label: "Labour", color:'#A3A3A3'}
						];

			var dataObj = {
				title:{
					text: "Average Household Income by Activity Category"
				},
				animationEnabled: true,
				legend:{
					verticalAlign: "center",
					horizontalAlign: "left",
					fontSize: 20,
					fontFamily: "Helvetica"        
				},
				theme: "theme2",
				data: [
					{        
						type: "pie",       
						indexLabelFontFamily: "verdana",       
						indexLabelFontSize: 12,
						indexLabel: "{label} {y}%",
						startAngle:-20,      
						toolTipContent:"{label} {y}%",
						dataPoints: dataPointsArray
					}
				]
			};

			renderChart(dataObj);

		},

		// Monthly Rainfall and Feed Availability [STACKED COLUMN CHART WITH RAINFALL SERIES REPRESENTED AS A LINE - RAIN ON AN AXIS FROM 0 TO 5, OTHER SERIES ON AN AXIS FROM 0 TO 10]
		monthly_rainfall: function(err, rows) {

			console.log("=========> ROWS");
			console.log(rows);
			
			var rainfallPts = [],
				ccrPts = [],
				concentratePts = [],
				grazingPts = [],
				greenForagePts = [],
				lcrPts = [],
				otherPts = [];


			for (var i = 0; i < rows.length; i++) {
				row = rows[i];
				switch(row.resource_type){
					case "Rainfall":
						rainfallPts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});
						break;
					case "Cereal Crop Residues":
						ccrPts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});
						break;
					case "Concentrates":
						concentratePts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});
						break;
					case "Grazing":
						grazingPts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});						
						break;
					case "Green Forage (e.g., weeds, fodder crops, leaves)":
						greenForagePts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});					
						break;
					case "Leguminous Crop Residues":
						lcrPts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});						
						break;
					case "Other (Unspecified)":
						otherPts.push({x: getMonth(2015, row.name_of_month), y: row.numerical_value});
						break;
					default:
						
						break;
				}
			};

			var dataObj = {
					title: {
						text: "Rainfall and Feed Availability",
					},
					animationEnabled: true,
					height: 400,
					toolTip: {
						shared: true,
						content: function(e) {
							var str = '';
							var total = 0;
							var str3;
							var str2;
							for (var i = 0; i < e.entries.length; i++) {
								var str1 = "<span style= 'color:" + e.entries[i].dataSeries.color + "'> " + e.entries[i].dataSeries.name + "</span>: $<strong>" + e.entries[i].dataPoint.y + "</strong>  bn<br/>";
								total = e.entries[i].dataPoint.y + total;
								str = str.concat(str1);
							}
							str2 = "<span style = 'color:DodgerBlue; '><strong>" + (e.entries[0].dataPoint.x).getFullYear() + "</strong></span><br/>";
							total = Math.round(total * 100) / 100
							str3 = "<span style = 'color:Tomato '>Total:</span><strong> $" + total + "</strong> bn<br/>";

							return (str2.concat(str)).concat(str3);
						}
					},
					axisY: {
						title: "Availability of feed (0-10)",
						titleFontSize: 16
					},
					axisY2: {
						title: "Rainfall (0-5)",
						titleFontSize: 16
					},
					axisX: {
						valueFormatString: "MMMM",
						labelFontStyle: "italic",
						labelMaxWidth: 100,  
						labelWrap: true,
						labelAngle: -45
					},
					data: [{
						type: "stackedColumn",
						showInLegend: true,
						color: "#ED7D31",
						name: "Cereal Crop Residues",
						dataPoints: ccrPts
					},{
						type: "stackedColumn",
						showInLegend: true,
						color: "#A5A5A5",
						name: "Concentrates",
						dataPoints: concentratePts
					}, {
						type: "stackedColumn",
						showInLegend: true,
						name: "Grazing",
						color: "#FFC000",
						dataPoints: grazingPts
					}, {
						type: "stackedColumn",
						showInLegend: true,
						name: "Leguminous Crop Residues",
						color: "#4472C4",
						dataPoints: lcrPts
					},{
						type: "stackedColumn",
						showInLegend: true,
						name: "Other (Unspecified)",
						color: "#6FAD46",
						dataPoints: otherPts
					}, {
						type: "stackedColumn",
						showInLegend: true,
						name: "Planted Forage, Tree Leaves, Etc",
						color: "#255E91",
						dataPoints: greenForagePts
					},{
						type: "line",
						color: "#4D92CF",
						axisYType: "secondary",
						showInLegend: true,
						name: "Sum of rainfall",
						dataPoints: rainfallPts
					}]
				};
			console.log(dataObj);

			renderChart(dataObj);

		},

		// Monthly Milk Yield versus Average Price Received  [LINE GRAPH - TWO DIFFERENT AXES FOR YIELD AND PRICE]
		monthly_milk_yield: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);

			var dataObj = {
				title: {
					text: "Average Monthly Milk Yield (L) vs. Avg. Price Received per Litre"
				},
				animationEnabled: true,
				axisX: {
					intervalType: "month",
					valueFormatString: "MMMM",
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				axisY: {
					title: "Yield (L)",
				},
				axisY2: {
					title: "Price (USD)",
				},
				data: [
				{
					type: "line",
					color: "#78AFE6",
					showInLegend: true,
					name: "Sum of average_yield",
					dataPoints: [ //array
						{
							x: new Date(2015, 00),
							y: 2.3,
						}, {
							x: new Date(2015, 01),
							y: 4
						}, {
							x: new Date(2015, 02),
							y: 7.6,

						}, {
							x: new Date(2015, 03),
							y: 8,

						}, {
							x: new Date(2015, 04),
							y: 5.5,

						}, {
							x: new Date(2015, 05),
							y: 1.2,

						}, {
							x: new Date(2015, 06),
							y: 2.2,

						}, {
							x: new Date(2015, 07),
							y: 2.2,

						}, {
							x: new Date(2015, 08),
							y: 1.2,

						}, {
							x: new Date(2015, 09),
							y: 6.5,
						}, {
							x: new Date(2015, 10),
							y: 5.5,
						}, {
							x: new Date(2015, 11),
							y: 8.1,
						}
					]
				},{
					type: "line",
					color: "#FF9900",
					showInLegend: true,
					axisYType: "secondary",
					name: "Sum of price_litre_milk_usd",
					dataPoints: [ //array
						{
							x: new Date(2015, 00),
							y: 400,
						}, {
							x: new Date(2015, 01),
							y: 500
						}, {
							x: new Date(2015, 02),
							y: 156,

						}, {
							x: new Date(2015, 03),
							y: 895,

						}, {
							x: new Date(2015, 04),
							y: 450,

						}, {
							x: new Date(2015, 05),
							y: 243,

						}, {
							x: new Date(2015, 06),
							y: 685,

						}, {
							x: new Date(2015, 07),
							y: 161,

						}, {
							x: new Date(2015, 08),
							y: 420,

						}, {
							x: new Date(2015, 09),
							y: 240,
						}, {
							x: new Date(2015, 10),
							y: 778,
						}, {
							x: new Date(2015, 11),
							y: 920,
						}
					]
				}
				]
			};

			renderChart(dataObj);

		},

		// Monthly Market Prices for Livestock [LINE GRAPH - TWO DIFFERENT AXES FOR HEADCOUNT AND PRICE]
		monthly_market_prices: function(err, rows) {
			console.log("=========> ROWS");
			console.log(rows);

			var dataObj = {
				title: {
					text: "Average Price of Major Livestock Species in USD by Month"
				},
				animationEnabled: true,
				axisX: {
					intervalType: "month",
					valueFormatString: "MMMM",
					labelFontStyle: "italic",
					labelMaxWidth: 100,  
					labelWrap: true,
					labelAngle: -45
				},
				axisY: {
					title: "Price Cattle (USD)",
				},
				axisY2: {
					title: "Price Goat (USD)",
				},
				data: [
				{
					type: "line",
					color: "#47B56C",
					showInLegend: true,
					name: "Average Price Cattle",
					dataPoints: [ //array
						{
							x: new Date(2015, 00),
							y: 45,
						}, {
							x: new Date(2015, 01),
							y: 38
						}, {
							x: new Date(2015, 02),
							y: 96,

						}, {
							x: new Date(2015, 03),
							y: 20,

						}, {
							x: new Date(2015, 04),
							y: 45,

						}, {
							x: new Date(2015, 05),
							y: 41,

						}, {
							x: new Date(2015, 06),
							y: 33,

						}, {
							x: new Date(2015, 07),
							y: 25,

						}, {
							x: new Date(2015, 08),
							y: 55,

						}, {
							x: new Date(2015, 09),
							y: 78,
						}, {
							x: new Date(2015, 10),
							y: 41,
						}, {
							x: new Date(2015, 11),
							y: 55,
						}
					]
				},{
					type: "line",
					color: "#78AFE6",
					showInLegend: true,
					name: "Average Price Sheep",
					dataPoints: [ //array
						{
							x: new Date(2015, 00),
							y: 12,
						}, {
							x: new Date(2015, 01),
							y: 17
						}, {
							x: new Date(2015, 02),
							y: 53,

						}, {
							x: new Date(2015, 03),
							y: 35,

						}, {
							x: new Date(2015, 04),
							y: 61,

						}, {
							x: new Date(2015, 05),
							y: 21,

						}, {
							x: new Date(2015, 06),
							y: 90,

						}, {
							x: new Date(2015, 07),
							y: 78,

						}, {
							x: new Date(2015, 08),
							y: 44,

						}, {
							x: new Date(2015, 09),
							y: 22,
						}, {
							x: new Date(2015, 10),
							y: 42,
						}, {
							x: new Date(2015, 11),
							y: 85,
						}
					]
				},{
					type: "line",
					color: "#FF9900",
					showInLegend: true,
					axisYType: "secondary",
					name: "Average Price Goat",
					dataPoints: [ //array
						{
							x: new Date(2015, 00),
							y: 400,
						}, {
							x: new Date(2015, 01),
							y: 500
						}, {
							x: new Date(2015, 02),
							y: 156,

						}, {
							x: new Date(2015, 03),
							y: 895,

						}, {
							x: new Date(2015, 04),
							y: 450,

						}, {
							x: new Date(2015, 05),
							y: 243,

						}, {
							x: new Date(2015, 06),
							y: 685,

						}, {
							x: new Date(2015, 07),
							y: 161,

						}, {
							x: new Date(2015, 08),
							y: 420,

						}, {
							x: new Date(2015, 09),
							y: 240,
						}, {
							x: new Date(2015, 10),
							y: 778,
						}, {
							x: new Date(2015, 11),
							y: 920,
						}
					]
				}
				]
			};

			renderChart(dataObj);

		},

		// Gender Pay Equality [COLUMN CHART]
		gender_pay_equality: function(err, rows) {
				console.log("=========> ROWS");
				console.log(rows);
				
				var payData = [
					{
						type: "column",	
						name: "Average Pay Female",
						legendText: "Average Pay Female",
						showInLegend: true,
						color: "#70AD47",
						dataPoints: [{
							label: "Total",
							y: rows[0].avg_pay_female
						}]
					},{
						type: "column",	
						name: "Average Pay Male",
						legendText: "Average Pay Male",
						showInLegend: true,
						color: "#4472C4",
						dataPoints: [{
							label: "Total",
							y: rows[0].avg_pay_male
						}]
					}
				];

				var dataObj = {
					theme: "theme2",
					animationEnabled: true,
					title:{
						text: "Average Daily Labour Rates by Gender (USD)",
						fontSize: 30
					},
					axisX:{
						margin: 10
					},
					axisY: {
						title: "Daily Labour Rate (USD)"
					},
					legend:{
						verticalAlign: "top",
						horizontalAlign: "center"
					},
					data: payData,
					legend: {
						cursor: "pointer",
						itemclick: function(e) {
							if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
								e.dataSeries.visible = false;
							} else {
								e.dataSeries.visible = true;
							}
							chart.render();
						}
					}
				};

				renderChart(dataObj);

			}
			/*
			query_label:function(err, rows){

			//format rows into dataObj here

			renderChart(dataObj);

			}
			*/

	}

	renderChartData(query, paramType, paramVal);

	function renderChartData(query, paramType, paramVal) {
		if (query && queries[query]) {
			if (queries[query] && resultBindings[query]) {

				paramObj = buildFilter(paramType, paramVal);

				queries[query].query(paramObj.param, paramObj.groupParam, resultBindings[query]);

			} else {
				$('#chartContainer').empty();
			}
		}
	}

	function renderChart(dataObj) {
		chart = new CanvasJS.Chart("chartContainer", dataObj);
		chart.render();
	}

	//seems kind of redundant now but wanted to allow for multiple args at the time
	function buildFilter(paramType, paramVal) {
		var retVal;
		if (paramType === 'byFocusGroup') {
			retVal = {
				param: 'focus_group.id = ' + paramVal,
				groupParam: 'focus_group.id'
			};
		} else if (paramType === 'bySite') {
			retVal = {
				param: 'focus_group.id_site = ' + paramVal,
				groupParam: 'focus_group.id_site'
			};
		} else retVal = false;
		return retVal;
	}

	function fireStatement(statement, callback) {
		if (typeof callback === 'string') {
			var label = callback;
			callback = function(err, rows) {
				console.log('\nTESTING queries.' + label + ':');
				console.log(rows || ('ERROR WITH STATEMENT:\n' + statement));
			};
		}

		db.serialize(function() {
			db.all(statement, callback);
		});

	}

	//testing

	for (var x in queries) {
		queries[x].query(buildFilter('bySite', 2).param, buildFilter('bySite', 2).groupParam, x);
	}

	function chartUi(ddData) {

		$('#chartContainer').on('newChartData', function() {
			renderCurrentSelections();
		});


		var $chartSelection = $('#chart_selection');
		var $projectSelection = $('#project_selection');
		var $siteSelection = $('#site_selection');
		var $groupSelection = $('#group_selection');
		var $allSelects = $('#chart_selection, #project_selection, #site_selection, #group_selection');
		var $btnRefresh = $('#btn_refresh');
		var $btnByBroup = $('#btn_by_group');
		var $btnBySite = $('#btn_by_site');

		var sid, pid, fid;

		$('#btn_refresh').click(function() {
			$('#chartContainer').trigger('newChartData');
		});

		var optionArr = [];
		for (var x in queries) {
			optionArr.push(['<option value="', x, '">', queries[x].displayName, '</option>'].join(''));
		}

		$chartSelection.html(optionArr.join(''));

		function buildOptions(idLabel, nameLabel, data) {
			var arr = [];
			data.forEach(function(el) {
				arr.push(['<option value="', el[idLabel], '">', el[nameLabel], '</option>'].join(''));
			});
			return arr.join('');
		}

		console.log(ddData);

		$projectSelection.html(buildOptions('project_id', 'project_name', ddData.projects));


		$projectSelection.change(function(e) {
			if (e.target.value !== pid) {
				pid = e.target.value;
				var data = ddData[pid].sites;
				$siteSelection.html(buildOptions('site_id', 'site_name', data));
				$siteSelection.change();
			} else {
				renderCurrentSelections();
			}
		});

		$siteSelection.change(function(e) {
			if (e.target.value !== sid) {
				sid = e.target.value;
				var data = ddData[pid][sid].focusGroups;
				$groupSelection.html(buildOptions('focus_group_id', 'focus_group_name', data));
				$groupSelection.change();
			} else {
				renderCurrentSelections();
			}
		});

		$groupSelection.change(function(e) {
			if (e.target.value !== fid) {
				fid = e.target.value;
				renderCurrentSelections();
			}
		});

		$projectSelection.change();

		var firstChart = $chartSelection[0].value;

		$('#btn_by_group, #btn_by_site').click(function(e) {
			if (!$(this).hasClass('active')) {
				var targetId = e.target.id;
				$('button.active').removeClass('active');
				$('#' + targetId).addClass('active');
				renderCurrentSelections();
			}
		});

		$chartSelection.change(function(e) {

			renderCurrentSelections();
		});

		function renderCurrentSelections() {
			var $activeBtn = $('#btn_by_group.active, #btn_by_site.active');
			var activeId = $activeBtn[0].id;

			var query = $chartSelection[0].value;
			var paramType = (activeId === 'btn_by_group') ? 'byFocusGroup' : 'bySite';
			var paramVal = (paramType === 'byFocusGroup') ? fid : sid;

			renderChartData(query, paramType, paramVal);
		}

	} //end chartUi

}; //end onload
//UIs
